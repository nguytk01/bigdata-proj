package HoltzWinters;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

public class CountByHour {
  public static class MyMapperPhase1 extends Mapper<Object, Text, Text, Text> {
    private static int ID = 0;
    private static int Case_Number = 1;
    private static int Date = 2;
    private static int Block = 3;
    private static int IUCR = 4;
    private static int Primary_Type = 5;
    private static int Description = 6;
    private static int Location_Description = 7;
    private static int Arrest = 8;
    private static int Domestic = 9;
    private static int Beat = 10;
    private static int District = 11;
    private static int Ward = 12;
    private static int Community_Area = 13;
    private static int FBI_Code = 14;
    private static int X_Coordinate = 15;
    private static int Y_Coordinate = 16;
    private static int Year = 17;
    private static int Updated_On = 18;
    private static int Latitude = 19;
    private static int Longitude = 20;
    static int columnSize = 0;
    private CSVRecord row;
    private CSVParser parser;
    private DateTimeFormatter dateTimeParser;
    private DateTimeFormatter dayOfYearFormatter;
    private DateTimeFormatter yearFormatter;
    private DateTimeFormatter hourFormatter;
    private int minYear;
    private int maxYear;
    private List<Integer> yearArray;

    @Override
    public void setup(Context context) throws IOException, InterruptedException {
      dateTimeParser = DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm:ss a");
      dayOfYearFormatter = DateTimeFormatter.ofPattern("DDD");
      hourFormatter = DateTimeFormatter.ofPattern("HH");
      yearFormatter = DateTimeFormatter.ofPattern("yyyy");
      yearArray = new java.util.ArrayList<Integer>();
      minYear = 10000;
      maxYear = -1;
    }

    public void map(Object key, Text value, Context context)
        throws IOException, InterruptedException {
      System.out.println(value);
      parser =
          new CSVParser(
              new java.io.StringReader(value.toString()),
              CSVFormat.RFC4180.withRecordSeparator("\n").withIgnoreEmptyLines(true));
      List<CSVRecord> csvRecordList = parser.getRecords();
      row = csvRecordList.get(0);
      // System.out.println(row.toString());

      // the entry we are parsing is the header
      if (row.get(Date).equals("Date")) return;
      TemporalAccessor dateTime = dateTimeParser.parse(row.get(Date));
      int dayOfYear = Integer.parseInt(dayOfYearFormatter.format(dateTime));
      int year = Integer.parseInt(yearFormatter.format(dateTime));
      boolean leapYear = year % 4 == 0 ? true : false;
      int hour = Integer.parseInt(hourFormatter.format(dateTime));

      if (year < minYear) {
        minYear = year;
      }

      if (year >= maxYear) {
        maxYear = year;
      }

      if (leapYear) {
        if (dayOfYear == 32) {
          return;
        }
        if (dayOfYear > 32) {
          dayOfYear = dayOfYear - 1;
        }
      }

      hour = hour + (dayOfYear) * 24 + (year - 2001) * 8670;

      String[] strArr =
          new String[] {
            new Integer(hour).toString(), new Integer(year).toString(),
          };
      context.write(new Text(String.join(",", strArr)), new Text(""));
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
      for (int year = minYear; year <= maxYear; year++) {
        for (int i = 0; i < 24 * 365; i++) {
          context.write(
              new Text(
                  String.join(
                      ",", new String[] {new Integer(i).toString(), new Integer(year).toString()})),
              new Text("1"));
        }
      }
    }
  }

  public static class MyReducerPhase1 extends Reducer<Text, Text, Text, Text> {
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {}

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {}

    public void reduce(Text key, Iterable<Text> values, Context context)
        throws IOException, InterruptedException {
      int count = 0;
      for (Text t : values) {
        if (t.toString().length() == 0) count++;
      }
      context.write(key, new Text(new Integer(count).toString()));
    }
  }
}
