package HoltzWinters;

import java.io.IOException;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

public class AverageByYear {
  public static class MyMapperPhase1 extends Mapper<Object, Text, Text, Text> {
    private static int ID = 0;
    private static int Case_Number = 1;
    private static int Date = 2;
    private static int Block = 3;
    private static int IUCR = 4;
    private static int Primary_Type = 5;
    private static int Description = 6;
    private static int Location_Description = 7;
    private static int Arrest = 8;
    private static int Domestic = 9;
    private static int Beat = 10;
    private static int District = 11;
    private static int Ward = 12;
    private static int Community_Area = 13;
    private static int FBI_Code = 14;
    private static int X_Coordinate = 15;
    private static int Y_Coordinate = 16;
    private static int Year = 17;
    private static int Updated_On = 18;
    private static int Latitude = 19;
    private static int Longitude = 20;
    private static int Location = 21;
    private static int Historical_Wards_20032015 = 22;
    private static int Zip_Codes = 23;
    private static int Community_Areas = 24;
    private static int Census_Tracts = 25;
    private static int Wards = 26;
    private static int Boundaries_ZIP_Codes = 27;
    private static int Police_Districts = 28;
    static int columnSize = 0;
    private CSVRecord row;
    private CSVParser parser;

    @Override
    public void setup(Context context) throws IOException, InterruptedException {}

    public void map(Object key, Text value, Context context)
        throws IOException, InterruptedException {
      String[] keyAndValue = value.toString().split("\t");
      String[] hourAndYear = keyAndValue[0].split(",");
      String[] strArr = new String[] {hourAndYear[0], keyAndValue[1]};
      context.write(new Text(hourAndYear[1]), new Text(String.join(",", strArr)));
    }
  }

  public static class MyReducerPhase1 extends Reducer<Text, Text, Text, Text> {
    private java.util.ArrayList<java.util.List<Number>> hourCountTupleList;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      hourCountTupleList = new java.util.ArrayList<java.util.List<Number>>();
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {}

    public void reduce(Text key, Iterable<Text> values, Context context)
        throws IOException, InterruptedException {
      java.util.Iterator<Text> iter1 = values.iterator();
      double yearSum = 0;
      String year = key.toString();
      while (iter1.hasNext()) {
        String valueOnEachRow = iter1.next().toString();
        String[] hourAndCount = valueOnEachRow.split(",");
        Double crimeCount = Double.parseDouble(hourAndCount[1]);
        int hour = Integer.parseInt(hourAndCount[0]);
        yearSum += crimeCount;
        java.util.List<Number> hourCountTuple = new java.util.ArrayList<Number>();
        hourCountTuple.add(hour);
        hourCountTuple.add(crimeCount);
        hourCountTupleList.add(hourCountTuple);
      }
      for (int i = 0; i < hourCountTupleList.size(); i++) {
        double average = (Double) hourCountTupleList.get(i).get(1) / yearSum;
        context.write(
            new Text(hourCountTupleList.get(i).get(0).toString() + "," + year),
            new Text(
                hourCountTupleList.get(i).get(1).toString()
                    + ","
                    + String.format("%.9f", average)));
      }
    }
  }
}
