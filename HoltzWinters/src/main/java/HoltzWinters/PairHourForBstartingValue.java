package HoltzWinters;

import java.io.IOException;
import java.util.List;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

public class PairHourForBstartingValue {
  public static class MyMapperPhase1 extends Mapper<Object, Text, Text, Text> {
    static int columnSize = 0;
    private CSVRecord row;
    private CSVParser parser;

    @Override
    public void setup(Context context) throws IOException, InterruptedException {}

    public void map(Object key, Text value, Context context)
        throws IOException, InterruptedException {
      String[] keyAndCount = value.toString().split("\t");
      String[] hourAndYear = keyAndCount[0].split(",");
      String[] strArr = new String[] {hourAndYear[1], keyAndCount[1]};
      if (!hourAndYear[1].equals("2001") && !hourAndYear[1].equals("2002")) return;

      context.write(new Text(hourAndYear[0]), new Text(String.join(",", strArr)));
    }
  }

  public static class MyReducerPhase1 extends Reducer<Text, Text, Text, DoubleWritable> {
    private java.util.ArrayList<Double> trendComponents;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      trendComponents = new java.util.ArrayList<Double>();
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
      double sum = 0;
      for (int i = 0; i < trendComponents.size(); i++) {
        sum += trendComponents.get(i);
      }
      sum = sum / 8760;
      context.write(new Text(""), new DoubleWritable(sum));
    }

    public void reduce(Text key, Iterable<Text> values, Context context)
        throws IOException, InterruptedException {
      java.util.Iterator<Text> iter1 = values.iterator();
      double yearSum = 0;
      String year = key.toString();
      List<String> list = new java.util.ArrayList<String>();
      while (iter1.hasNext()) {
        list.add(iter1.next().toString());
      }
      list.sort((String a, String b) -> a.compareTo(b));
      double result =
          (Integer.parseInt(list.get(0).split(",")[1])
                  - Integer.parseInt(list.get(0).split(",")[2]))
              / ((double) 8760);
      trendComponents.add(result);
    }
  }
}
