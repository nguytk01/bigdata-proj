package HoltzWinters;

public class App {
  public static void main(String[] args) throws Exception {
    int subTask = Integer.parseInt(args[1]);
    org.apache.hadoop.util.Tool tool = new ProjTool(subTask);
    tool.run(args);
  }
}
