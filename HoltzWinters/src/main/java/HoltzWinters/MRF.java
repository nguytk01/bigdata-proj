package HoltzWinters;

import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;

import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

public class MRF {
  public static class MyMapperPhase1 extends Mapper<Object, Text, Text, Text> {

    @Override
    public void setup(Context context) throws IOException, InterruptedException {}

    public void map(Object key, Text value, Context context)
        throws IOException, InterruptedException {
      int w = 10;
      String[] keyAndCount = value.toString().split("\t");
      String[] hourAndYear = keyAndCount[0].split(",");
      int hour = Integer.parseInt(hourAndYear[0]);
      if ( hour < 1 ) return;
      for (int windowSize = 2; windowSize < w; windowSize++) {
        int deltaj = (windowSize * (windowSize + 1)) / 2;
        /* deltaj = 1 + 2 + 3 + ... + deltaj */
        /* at windowSize.(windowSize - 1), weight is 1
          Ex: at item 3, windowsSize 2, 2.(2-1) or 2.1, weight is 1
        */
        context.write(
            new Text(new Integer(windowSize).toString() + "|" + new Integer(hour - 1).toString()),
            new Text(new Integer(keyAndCount[1]).toString() + ",0"));

        for (int count = 2; count <= windowSize; count++) {
          if ( count >= hour ) continue;
          /* windowSize, windowSize - 1, windowSize - 2, ... */
          String kprime =
              new Integer(windowSize).toString() + "|" + new Integer(hour - count).toString();
          double weight = ((double) count) / deltaj;
          String vprime = new Double(weight * Integer.parseInt(keyAndCount[1])).toString();
          context.write(new Text(kprime), new Text(vprime + ",1"));
        }
      }
    }
  }

  //public static class MyReducerPhase1 extends Reducer<Text, Text, Text, Text> {
    public static class MyReducerPhase1 extends Reducer<Text, Text, IntWritable, DoubleWritable> {
    
    private double[] meanSquareErrorList;
    private double[] meanSquareErrorSumCountList;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      meanSquareErrorList = new double[10];
      meanSquareErrorSumCountList = new double[10];
      java.util.Arrays.fill(meanSquareErrorList, 0);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
      double min = Double.MAX_VALUE;
      int minIndex = -1;
      for (int i = 2; i < meanSquareErrorList.length; i++) {
        context.write(
            new IntWritable(i),
            new DoubleWritable(meanSquareErrorList[i] / meanSquareErrorSumCountList[i]));

        if (meanSquareErrorList[i] / meanSquareErrorSumCountList[i] < min) {
          min = meanSquareErrorList[i] / meanSquareErrorSumCountList[i];
          minIndex = i;
        }
      }
      context.write(new IntWritable(minIndex), new DoubleWritable(min));
    }

    public void reduce(Text key, Iterable<Text> values, Context context)
        throws IOException, InterruptedException {
      double firstComponent = 0;
      double secondComponent = 0;
      for (Text t : values) {
        String[] valueAndTag = t.toString().split(",");
        double value = Double.parseDouble(valueAndTag[0]);
        String tag = valueAndTag[1];
        if (tag.equals("0")) {
          firstComponent = value;
          //context.write(key, t);
        } else {
          secondComponent += value;
          //context.write(t, new Text("111"));
          //context.write(key, t);
        }
      }
      // context.write(new IntWritable(99999), new DoubleWritable(99999));
      String[] windowSizeAndStartIndex = key.toString().split("|");
      int windowSize = Integer.parseInt(windowSizeAndStartIndex[0]);
      meanSquareErrorList[windowSize] += Math.pow(firstComponent - secondComponent, 2);
      meanSquareErrorSumCountList[windowSize] += 1;
    }
  }
}
