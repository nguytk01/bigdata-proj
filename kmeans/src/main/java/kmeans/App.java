/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package kmeans;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Encoders;
import org.apache.spark.ml.clustering.KMeans;
import org.apache.spark.ml.clustering.KMeansModel;
import org.apache.spark.ml.linalg.DenseVector;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.rdd.RDD;
import kmeans.App.Payload;
public class App {
  public static class Payload implements java.io.Serializable{
    public double[] features;
    public Payload (double xcoord, double ycoord) {
      features = new double[] {xcoord, ycoord};
    }
    public double[] getFeatures(){
      return features;
    }
    public void setFeatures(double[] featuresIn) {
      features = featuresIn;
    }
  }
  public static void main(String[] args) {
    String inputFilePath = "bigdata-proj/withEverything-r-00000"; // Should be some file on your system
    SparkSession spark = SparkSession.builder().appName("K Means").getOrCreate();
    Dataset<String> dataset = spark.read().textFile(inputFilePath).cache();
    Dataset<Payload> payloadDataset = dataset.map(
      new MapFunction<String, Payload> () {
        public Payload call (String input) throws Exception{
          String[] keyValue = input.split("\\t");
          String[] valueList = keyValue[1].split("=====");
          return new Payload(Double.parseDouble(valueList[6]), Double.parseDouble(valueList[7]));
        }
      }, Encoders.bean(Payload.class)
    );
    int numOfInputClusters = Integer.parseInt(args[0]);
    payloadDataset.show();
    KMeans kmeans = new KMeans();
    kmeans.setK(numOfInputClusters);
    KMeansModel model = kmeans.fit(payloadDataset);
    Vector[] vector = model.clusterCenters();
    Dataset<Row> predictions = model.transform(payloadDataset);
    predictions.rdd().saveAsTextFile("bigdata-proj-kmeans-output/");
  }
}
