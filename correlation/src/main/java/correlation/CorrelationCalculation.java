package correlation;

import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Encoders;
import org.apache.spark.ml.clustering.KMeans;
import org.apache.spark.ml.clustering.KMeansModel;
import org.apache.spark.ml.linalg.DenseVector;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.ml.linalg.Vectors;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.Column;
import org.apache.spark.api.java.function.ForeachFunction;
import org.apache.spark.ml.stat.Correlation;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.ml.linalg.VectorUDT;
import org.apache.spark.sql.catalyst.encoders.ExpressionEncoder;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;

public class CorrelationCalculation {
  public static int NUM_OF_TIME_UNITS_PER_YEAR = 12;
  public static class KeyValue{
    public int key;
    public int value;
    public KeyValue (int keyIn, int valueIn){
      key = keyIn;
      value = valueIn;
    }
    public int getKey() { return key;} public int getValue() { return value;}
    public void setKey(int keyIn) {key=keyIn;} public void setValue(int valueIn) { value = valueIn;};
  } 
  public static class Payload implements java.io.Serializable{
    public String label;
    public String backend;

    public Payload(String labelIn, String backendIn) {
      label = labelIn;
      backend = backendIn;
    }
   
    public String getLabel(){return label;} public void setLabel(String in) {label = in;};
    public String getBackend(){return backend;} public void setBackend(String backendIn) {backend = backendIn;};

  }
  
  public static void main(String[] args) {
    String inputFilePath1 = "bigdata-proj/correlation/CommWeekCrimeCountCorrelation/out.txt/part-00000"; // Should be some file on your system
    //String inputFilePath2 = "bigdata-proj/correlation/CommWeekCrimeCountCorrelation/out1.txt/part-00000"; // Should be some file on your system
    //String inputFilePath3 = "bigdata-proj/correlation/CommWeekCrimeCountCorrelation/out2.txt/part-00000"; // Should be some file on your system

    SparkSession spark = SparkSession.builder().appName("correlation calculation").getOrCreate();
    //spark.sparkContext().setLogLevel("ERROR");
    int NUM_OF_TIME_UNITS_PER_YEAR = Integer.parseInt(args[0]);
    List<Dataset<String>> datasetStringList = new ArrayList<Dataset<String>>();
    datasetStringList.add(spark.read().textFile(inputFilePath1));
    //datasetStringList.add(spark.read().textFile(inputFilePath2));
    //datasetStringList.add(spark.read().textFile(inputFilePath3));
    List<Dataset<Payload>> datasetRowList = new ArrayList<Dataset<Payload>>();
    for (Dataset<String> dataset : datasetStringList) {
      datasetRowList.add(dataset.map(
      new MapFunction<String, Payload> () {
        public Payload call (String input) throws Exception{
          String correctStr = input.substring(1,input.length());
          correctStr = correctStr.substring(0, input.length() - 2);
          String[] numStrList = correctStr.split(",");
          String label = numStrList[0];
          String[] features = new String[numStrList.length - 1];
          for (int i = 0; i < features.length; i++) {
            features[i] = numStrList[i+1];
          }
          return new Payload(label, String.join(",", features));
        }
      }, Encoders.bean(Payload.class)));
    }
    Dataset<Payload> combinedDataset = datasetRowList.get(0);//.join(datasetRowList.get(1), "label").join(datasetRowList.get(2),"label");
    combinedDataset.show();
    
    StructType schema = new StructType(new StructField[]{
      new StructField("features", new VectorUDT(), false, Metadata.empty()),
    });

    ExpressionEncoder<Row> encoder = RowEncoder.apply(schema);
    Dataset<Row> data = combinedDataset.map(
      new MapFunction<Payload, Row> () {
        public Row call (Payload input) throws Exception{
          String p1 = (String)(input.getBackend());
          //String p2 = (String)(input.get(2));
          //String p3 = (String)(input.get(3));
          //String[] str = ArrayUtils.addAll(p1.split(","), p2.split(","));
          //str = ArrayUtils.addAll(str,p3.split(","));
	  String[] str = p1.split(",");
          double[] features = new double[str.length];
          for (int i = 1 ; i < str.length; i++) {
            features[i] = Double.parseDouble(str[i]);
          }
          return RowFactory.create(Vectors.dense(features));
        }
      }, encoder
      );
    data.show();
    data.printSchema();
    Row r1 = Correlation.corr(data, "features").head();
    org.apache.spark.ml.linalg.Matrix matrix = (org.apache.spark.ml.linalg.Matrix) (r1.get(0));
    System.out.println(r1.get(0).toString());
    for (int i = 0 ; i < matrix.numRows(); i++) {
      for (int j = 0 ; j < matrix.numCols(); j++ ){
        if ( matrix.apply(i,j) < 0.5) {
          System.out.println( matrix.apply(i,j) );
        }
      }
    }
}
}
