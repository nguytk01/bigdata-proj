package correlation;

import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Encoders;
import org.apache.spark.ml.clustering.KMeans;
import org.apache.spark.ml.clustering.KMeansModel;
import org.apache.spark.ml.linalg.DenseVector;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.Column;
import org.apache.spark.api.java.function.ForeachFunction;

public class CorrelationSpark {
  public static int NUM_OF_TIME_UNITS_PER_YEAR =12;
  public static class KeyValue{
    public int key;
    public int value;
    public KeyValue (int keyIn, int valueIn){
      key = keyIn;
      value = valueIn;
    }
    public int getKey() { return key;} public int getValue() { return value;}
    public void setKey(int keyIn) {key=keyIn;} public void setValue(int valueIn) { value = valueIn;};
  }
  
  public static class Num{
    public int num; public Num( int numIn) { num = numIn;} public int getNum() { return num;} public void setNum(int numIn) {num = numIn;}
  }
  public static class Payload implements java.io.Serializable{
    public int commArea;
    public int timeUnit;
    public int absTimeUnit;
    public int year;
    public String type;
    public int count;
    public Payload (String input) {
      String[] keyValue = input.split("\\t");
      String[] keyList = keyValue[0].split(",");
      commArea = Integer.parseInt(keyList[0]);
      timeUnit = Integer.parseInt(keyList[1]);
      year = Integer.parseInt(keyList[2]);
      type = keyList[3];
      count = Integer.parseInt(keyValue[1]);
      absTimeUnit = timeUnit + NUM_OF_TIME_UNITS_PER_YEAR * (year - 2000);
    }
    public Payload(int commAreaIn, int timeUnitIn, int yearIn, String typeIn, int countIn) {
      commArea = commAreaIn;
      timeUnit = timeUnitIn;
      year = yearIn;
      type = typeIn;
      absTimeUnit = timeUnit + NUM_OF_TIME_UNITS_PER_YEAR * (year - 2000);
    }
    public int getCommArea(){return commArea;} public void setCommArea(int in) {commArea = in;};
    public int getTimeUnit(){return timeUnit;} public void setTimeUnit(int in) {timeUnit = in;};
    public int getYear(){return year;}  public void setYear(int in) {year = in;};
    public String getType(){return type;} public void setType(String in) {type = in;};
    public int getCount(){return count;} public void setCount(int in) {count = in;};
    public int getAbsTimeUnit(){return absTimeUnit;} public void setAbsTimeUnit(int in) {absTimeUnit = in;};
  }
  public static void main(String[] args) {
    String inputFilePath = "bigdata-proj/correlation/CommWeekCrimeCount/part-r-00000"; // Should be some file on your system
    SparkSession spark = SparkSession.builder().appName("correlation").getOrCreate();
    //spark.sparkContext().setLogLevel("ERROR");
    int NUM_OF_TIME_UNITS_PER_YEAR = Integer.parseInt(args[0]);
    Dataset<String> dataset = spark.read().textFile(inputFilePath).cache();
    Dataset<String> uniqueCrimeType = dataset.map(
      new MapFunction<String, String> () {
        public String call (String input) throws Exception{
          String[] keyValue = input.split("\\t");
          String[] keyList = keyValue[0].split(",");
          return keyList[3];
        }
      }, Encoders.STRING()
      ).distinct();
    Dataset<String> uniqueCommunityAreas = dataset.map(
      new MapFunction<String, String> () {
        public String call (String input) throws Exception{
          String[] keyValue = input.split("\\t");
          String[] keyList = keyValue[0].split(",");
          return keyList[0];
        }
      }, Encoders.STRING()
      ).distinct();
    Dataset<Payload> payloadDataset = dataset.map(
      new MapFunction<String, Payload> () {
        public Payload call (String input) throws Exception{
          return new Payload(input);
        }
      }, Encoders.bean(Payload.class)
      );
    String crime= "THEFT";
    //payloadDataset.show();
    
    int minYear = 2000;
    int maxYear = 2018;
    int numOfPeriodsPerYear = 52;
    java.util.List<Num> fullList = new java.util.ArrayList<Num>();
      for (int i = minYear; i<= maxYear; i++) {
        for ( int j = 0; j < numOfPeriodsPerYear; j++) {
          fullList.add(new Num( j + NUM_OF_TIME_UNITS_PER_YEAR * (i - 2000)));
        }
      }
    Dataset<Num> fullZeroDataset = spark.createDataset(fullList, Encoders.bean(Num.class));
    //fullZeroDataset.show();
    //payloadDataset.select(payloadDataset.col("absTimeUnit")).except(fullZeroDataset.select(fullZeroDataset.col("num"))).show();
    java.util.List<Dataset<Row>> list = new java.util.ArrayList<Dataset<Row>>();
    for (int i= 1 ; i <= 77; i++) {
      Dataset<Row> eachDataset =
        payloadDataset
        .filter(payloadDataset.col("commArea").equalTo(i))
        .filter(payloadDataset.col("type").equalTo(crime))
        .filter(payloadDataset.col("year").lt(new Integer(2019)))
        .select(payloadDataset.col("absTimeUnit"), payloadDataset.col("count"));
      //System.out.println("each dataset count " + eachDataset.count() + " " + eachDataset.distinct().count());
      //eachDataset.show();
      Dataset<Row> periodsNotInDataset = fullZeroDataset.select(fullZeroDataset.col("num")).except(eachDataset.select("absTimeUnit").toDF("num"));
      //periodsNotInDataset.show();
      Dataset<KeyValue> periodsNotInDatasetWithZero = periodsNotInDataset.map(
        new MapFunction<Row, KeyValue> () {
        public KeyValue call (Row input) throws Exception{
          return new KeyValue((Integer)input.get(0), 0);
        }
      }, Encoders.bean(KeyValue.class)
      );
      //periodsNotInDatasetWithZero.show();
      //System.out.println("Num of periods not in dataset " + periodsNotInDatasetWithZero.count());
      //System.out.println("Num of unique periods not in dataset " + periodsNotInDatasetWithZero.distinct().count());

      list.add(eachDataset.toDF("key","value").union(periodsNotInDatasetWithZero.toDF("key","value")).toDF("key", "value"+i));
    }
    //list.get(0).show();
    System.out.println(list.size());
    System.out.println();
    //System.out.println("result count " + list.get(0).count() + " full zero count " + fullZeroDataset.count());
    //list.get(0).select(list.get(0).col("key")).except(fullZeroDataset.select(fullZeroDataset.col("num"))).show();
    Dataset<Row> result = list.get(0);
    for (int i = 1 ; i < list.size(); i++) {
      //result = result.join(list.get(i), result.col("key").equalTo(list.get(i).col("key"+ (i+1) )), "inner");
      result = result.join(list.get(i), "key");
    }
    //result.show();
    result.coalesce(1).rdd().saveAsTextFile("bigdata-proj/correlation/CommWeekCrimeCountCorrelation/out.txt");
    //Dataset<Row> countDataset = payloadDataset.groupBy("type").sum("count");
    //countDataset.sort( countDataset.col("sum(count)").desc()).limit(15).show();
    //Dataset<Row> rowDataset = payloadDataset.toDF("features");
    //uniqueCrimeType.show();
    //uniqueCommunityAreas.show();
    
  }
}
