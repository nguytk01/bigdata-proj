package correlation;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Job;

  import org.apache.commons.csv.CSVFormat;
  import org.apache.commons.csv.CSVRecord;
  import org.apache.commons.csv.CSVParser;
  import java.util.List;


import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

public class AccusedComplaints {
public static class AccusedComplaintsMapper extends Mapper<LongWritable, Text, ImmutableBytesWritable, Put>{
  private CSVParser parser;
  private CSVRecord row;

    private static int row_id = 0;
private static int complaintsaccused_20002018_201803_ID = 1;
private static int cr_id = 2;
private static int accusation_id = 3;
private static int allegation_category = 4;
private static int allegation_category_code = 5;
private static int current_investigator_category = 6;
private static int current_investigator_category_code = 7;
private static int unit = 8;
private static int unit_detail = 9;
private static int accused_arrested = 10;
private static int duty_status = 11;
private static int injured = 12;
private static int final_finding = 13;
private static int finding_narrative = 14;
private static int penalty_id = 15;
private static int penalty_code = 16;
private static int number_of_days = 17;
private static int final_finding_narrative = 18;
private static int star = 19;
private static int rank = 20;
private static int final_finding_USE = 21;
private static int final_outcome_USE = 22;
private static int merge = 23;
private static int UID = 24;
  @Override
    public void setup(Context context) throws java.io.IOException, InterruptedException {}
  
  public void map(LongWritable offset, Text value, Mapper<LongWritable, Text, ImmutableBytesWritable, Put>.Context context) throws java.io.IOException, InterruptedException {
    List<CSVRecord> csvRecordList = new java.util.ArrayList<CSVRecord>();
    try {
    parser = new CSVParser(new java.io.StringReader(value.toString()), CSVFormat.RFC4180.withIgnoreEmptyLines(true));
    csvRecordList= parser.getRecords();
    } catch (Exception e) {
      return;
    }
    if (csvRecordList.size() == 0) {
      return;
    }
    row = csvRecordList.get(0);
    try {
      String test = row.get(final_finding_USE);
      test= row.get(allegation_category_code);
    } catch (Exception e ) {
      return;
    }
    if (row.get(row_id).equals("row_id")) return;

    String id = row.get(cr_id);
    Put put = new Put( Bytes.toBytes(id));
    put.addColumn(Bytes.toBytes("Accusation"), Bytes.toBytes("outcome"),  Bytes.toBytes(row.get(final_finding_USE)));
    put.addColumn(Bytes.toBytes("Accusation"), Bytes.toBytes("catCode"),  Bytes.toBytes(row.get(allegation_category_code)));

    context.write( new ImmutableBytesWritable(Bytes.toBytes(id)), put);
  }
}

public static class AccusedComplaintsReducer extends TableReducer<ImmutableBytesWritable, Put, ImmutableBytesWritable>{
  public void reduce
    (
      ImmutableBytesWritable row,
      Iterable<Put> putIterable,
      Reducer<ImmutableBytesWritable, Put,ImmutableBytesWritable, Put>.Context context
    ) throws java.io.IOException, InterruptedException
  {
    for (Put p : putIterable) {
      context.write(row, p);
    }
  }
}
}
