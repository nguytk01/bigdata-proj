package correlation;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVParser;
import java.util.List;



import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

public class CrimeEntryInput {
public static class ImportCrimeMapper extends Mapper<LongWritable, Text, ImmutableBytesWritable, Put>{
  private CSVParser parser;
  private CSVRecord row;
  private DateTimeFormatter dateTimeParser;
  private DateTimeFormatter dateFormatter;
  private DateTimeFormatter timeFormatter;
  private DateTimeFormatter weekFormatter;
  private DateTimeFormatter quarterFormatter;
  private DateTimeFormatter monthFormatter;

    private static int ID = 0;
    private static int Case_Number = 1;
    private static int Date = 2;
    private static int Block = 3;
    private static int IUCR = 4;
    private static int Primary_Type = 5;
    private static int Description = 6;
    private static int Location_Description = 7;
    private static int Arrest = 8;
    private static int Domestic = 9;
    private static int Beat = 10;
    private static int District = 11;
    private static int Ward = 12;
    private static int Community_Area = 13;
    private static int FBI_Code = 14;
    private static int X_Coordinate = 15;
    private static int Y_Coordinate = 16;
    private static int Year = 17;
    private static int Updated_On = 18;
    private static int Latitude = 19;
    private static int Longitude = 20;
  @Override
    public void setup(Context context) throws java.io.IOException, InterruptedException {
      dateTimeParser = DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm:ss a");
      dateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
      timeFormatter = DateTimeFormatter.ofPattern("hh:mm:ss a");
      weekFormatter = DateTimeFormatter.ofPattern("ww");
      monthFormatter = DateTimeFormatter.ofPattern("MM");
      quarterFormatter = DateTimeFormatter.ofPattern("QQ");
    }
  
  public void map(LongWritable offset, Text value, Mapper<LongWritable, Text, ImmutableBytesWritable, Put>.Context context) throws java.io.IOException, InterruptedException {
    parser = new CSVParser(new java.io.StringReader(value.toString()), CSVFormat.RFC4180.withRecordSeparator("\n").withIgnoreEmptyLines(true));
    List<CSVRecord> csvRecordList= parser.getRecords();
    if (csvRecordList.size() == 0) {
      return;
    }
    row = csvRecordList.get(0);
    if (row.get(Date).equals("Date")) return;
    //TemporalAccessor dateTime = dateTimeParser.parse(row.get(Date));
    long epochSeconds = java.time.LocalDateTime.parse(row.get(Date), dateTimeParser).toInstant(java.time.ZoneOffset.of(java.time.ZoneId.SHORT_IDS.get("EST"))).getEpochSecond();
    TemporalAccessor dateTime = dateTimeParser.parse(row.get(Date));

    String id = row.get(ID);
    String blockAddr = row.get(Block);
    Put put = new Put( Bytes.toBytes(id));
    if (row.get(Latitude).length() == 0 || row.get(Longitude).length() == 0 || blockAddr.length() == 0) return;
    put.addColumn(Bytes.toBytes("CrimeLocation"), Bytes.toBytes("Latitude"),  Bytes.toBytes(row.get(Latitude)));
    put.addColumn(Bytes.toBytes("CrimeLocation"), Bytes.toBytes("Longitude"),  Bytes.toBytes(row.get(Longitude)));
    put.addColumn(Bytes.toBytes("CrimeLocation"), Bytes.toBytes("Block"),  Bytes.toBytes(blockAddr.substring(0,5)));
    put.addColumn(Bytes.toBytes("CrimeLocation"), Bytes.toBytes("Street"),  Bytes.toBytes(blockAddr.substring(5,blockAddr.length()).trim()));
    put.addColumn(Bytes.toBytes("CrimeLocation"), Bytes.toBytes("CommArea"),  Bytes.toBytes(row.get(Community_Area)));

    put.addColumn(Bytes.toBytes("CrimeLocation"), Bytes.toBytes("LocationDesc"),  Bytes.toBytes(row.get(Location_Description)));

    put.addColumn(Bytes.toBytes("PoliceLocation"), Bytes.toBytes("Beat"),  Bytes.toBytes(row.get(Beat)));
    put.addColumn(Bytes.toBytes("PoliceLocation"), Bytes.toBytes("District"),  Bytes.toBytes(row.get(District)));
    put.addColumn(Bytes.toBytes("PoliceLocation"), Bytes.toBytes("Ward"),  Bytes.toBytes(row.get(Ward)));

    put.addColumn(Bytes.toBytes("Time"), Bytes.toBytes("seconds"),  Bytes.toBytes(epochSeconds));
    put.addColumn(Bytes.toBytes("Time"), Bytes.toBytes("year"),  Bytes.toBytes(row.get(Year)));
    put.addColumn(Bytes.toBytes("Time"), Bytes.toBytes("month"),  Bytes.toBytes(monthFormatter.format(dateTime)));
    put.addColumn(Bytes.toBytes("Time"), Bytes.toBytes("week"),  Bytes.toBytes(weekFormatter.format(dateTime)));
    put.addColumn(Bytes.toBytes("Time"), Bytes.toBytes("quarter"),  Bytes.toBytes(quarterFormatter.format(dateTime)));
    
    put.addColumn(Bytes.toBytes("Attr"), Bytes.toBytes("Arrest"),  Bytes.toBytes(new Boolean(row.get(Arrest).toLowerCase())));
    put.addColumn(Bytes.toBytes("Attr"), Bytes.toBytes("Type"),  Bytes.toBytes(row.get(Primary_Type)));
    put.addColumn(Bytes.toBytes("Attr"), Bytes.toBytes("Domestic"),  Bytes.toBytes(new Boolean(row.get(Domestic).toLowerCase())));

    context.write( new ImmutableBytesWritable(Bytes.toBytes(id)), put);
    //}
  }
}
public static class ImportCrimeReducer extends TableReducer<ImmutableBytesWritable, Put, ImmutableBytesWritable>{
  protected void reduce
    (
      ImmutableBytesWritable row,
      Iterable<Put> putIterable,
      Reducer<ImmutableBytesWritable, Put,ImmutableBytesWritable, Put>.Context context
    ) throws java.io.IOException, InterruptedException
  {
    for (Put p : putIterable) {
      context.write(row, p);
    }
  }
}
}
