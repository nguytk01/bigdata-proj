package correlation;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.csv.CSVParser;
import java.time.temporal.ChronoField;
import java.util.List;
import java.time.ZoneId;


import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

public class Complaints {
public static class ComplaintsMapper extends Mapper<LongWritable, Text, ImmutableBytesWritable, Put>{
  private CSVParser parser;
  private CSVRecord row;
  private DateTimeFormatter dateTimeParser;
  private DateTimeFormatter dateFormatter;
  private DateTimeFormatter timeFormatter;
  private DateTimeFormatter weekFormatter;
  private DateTimeFormatter quarterFormatter;
  private DateTimeFormatter monthFormatter;
  private DateTimeFormatter yearFormatter;

  private static int cr_id = 0;
  private static int cv = 1;
  private static int incident_date = 2;
  private static int complaint_date = 3;
  private static int closed_date = 4;
  private static int add1 = 5;
  private static int add2 = 6;
  private static int beat = 7;
  private static int city = 8;
  private static int full_address = 9;
  @Override
    public void setup(Context context) throws java.io.IOException, InterruptedException {
      dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
      weekFormatter = DateTimeFormatter.ofPattern("ww");
      monthFormatter = DateTimeFormatter.ofPattern("MM");
      quarterFormatter = DateTimeFormatter.ofPattern("QQ");
      yearFormatter = DateTimeFormatter.ofPattern("yyyy");
    }
  
  public void map(LongWritable offset, Text value, Mapper<LongWritable, Text, ImmutableBytesWritable, Put>.Context context) throws java.io.IOException, InterruptedException {
    parser = new CSVParser(new java.io.StringReader(value.toString()), CSVFormat.RFC4180.withRecordSeparator("\n").withIgnoreEmptyLines(true));
    List<CSVRecord> csvRecordList= parser.getRecords();
    if (csvRecordList.size() == 0) {
      return;
    }
    row = csvRecordList.get(0);
    try {
      String a = row.get(incident_date);
      a = row.get(complaint_date);
      a = row.get(closed_date);
    } catch ( Exception e ) {
      return;
    }
    if (row.get(closed_date).length() == 0 || row.get(city).length() == 0 || row.get(incident_date).length() == 0 || row.get(complaint_date).length() ==0 ) return;
    if (row.get(incident_date).equals("incident_date")) return;
    //TemporalAccessor dateTime = dateTimeParser.parse(row.get(Date));
    long incident_epochSeconds = java.time.LocalDate.parse(row.get(incident_date)).atStartOfDay(
      ZoneId.of(ZoneId.SHORT_IDS.get("EST"))).toInstant().getEpochSecond();
    long complaint_epochSeconds = java.time.LocalDate.parse(row.get(complaint_date)).atStartOfDay(
      ZoneId.of(ZoneId.SHORT_IDS.get("EST"))).toInstant().getEpochSecond();

    TemporalAccessor incidentDate = dateFormatter.parse(row.get(incident_date));
    TemporalAccessor complaintDate = dateFormatter.parse(row.get(complaint_date));
    int daysToSolve = 0;
    if (row.get(closed_date).length() > 0 ) {
      TemporalAccessor closedDate = dateFormatter.parse(row.get(closed_date));
      daysToSolve = 1;
    } else {
      daysToSolve = 0;
    }

    String id = row.get(cr_id);

    Put put = new Put( Bytes.toBytes(id));


    put.addColumn(Bytes.toBytes("Complaint"), Bytes.toBytes("beat"),  Bytes.toBytes(row.get(beat)));
    
    String incidentColumnFamily = "Incident";
    put.addColumn(Bytes.toBytes(incidentColumnFamily), Bytes.toBytes("seconds"),  Bytes.toBytes(incident_epochSeconds));
    put.addColumn(Bytes.toBytes(incidentColumnFamily), Bytes.toBytes("year"),  Bytes.toBytes(yearFormatter.format(incidentDate)));
    put.addColumn(Bytes.toBytes(incidentColumnFamily), Bytes.toBytes("month"),  Bytes.toBytes(monthFormatter.format(incidentDate)));
    put.addColumn(Bytes.toBytes(incidentColumnFamily), Bytes.toBytes("week"),  Bytes.toBytes(weekFormatter.format(incidentDate)));
    put.addColumn(Bytes.toBytes(incidentColumnFamily), Bytes.toBytes("quarter"),  Bytes.toBytes(quarterFormatter.format(incidentDate)));
    String complaintColumnFamily = "Complaint";

    put.addColumn(Bytes.toBytes(complaintColumnFamily), Bytes.toBytes("seconds"),  Bytes.toBytes(complaint_epochSeconds));
    put.addColumn(Bytes.toBytes(complaintColumnFamily), Bytes.toBytes("year"),  Bytes.toBytes(yearFormatter.format(complaintDate)));
    put.addColumn(Bytes.toBytes(complaintColumnFamily), Bytes.toBytes("month"),  Bytes.toBytes(monthFormatter.format(complaintDate)));
    put.addColumn(Bytes.toBytes(complaintColumnFamily), Bytes.toBytes("week"),  Bytes.toBytes(weekFormatter.format(complaintDate)));
    put.addColumn(Bytes.toBytes(complaintColumnFamily), Bytes.toBytes("quarter"),  Bytes.toBytes(quarterFormatter.format(complaintDate)));
    put.addColumn(Bytes.toBytes(complaintColumnFamily), Bytes.toBytes("daysToSolve"),  Bytes.toBytes(1));

    context.write( new ImmutableBytesWritable(Bytes.toBytes(id)), put);
    //}
  }
}
  public static class ComplaintsReducer extends TableReducer<ImmutableBytesWritable, Put, ImmutableBytesWritable>{
    protected void reduce
      (
        ImmutableBytesWritable row,
        Iterable<Put> putIterable,
        Reducer<ImmutableBytesWritable, Put,ImmutableBytesWritable, Put>.Context context
      ) throws java.io.IOException, InterruptedException
    {
      for (Put p : putIterable) {
        context.write(row, p);
      }
    }
  }
}
