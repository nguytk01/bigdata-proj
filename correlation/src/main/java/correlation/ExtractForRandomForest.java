package correlation;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.hbase.util.Bytes;
import java.util.NavigableMap;
import java.util.Map.Entry;
import java.util.ArrayList;
public class ExtractForRandomForest {
  public static class ExtractForRandomForestMapper
    extends TableMapper<Text,IntWritable>
  {
    private ArrayList<String> families;
    private ArrayList<String> columns;

    @Override
    public void setup(TableMapper<Text, IntWritable>.Context context) throws java.io.IOException, InterruptedException {
      String columnOrder = context.getConfiguration().get("columnOrder");
      families = new ArrayList<String>();
      columns = new ArrayList<String>();
      String[] columnStringArr = columnOrder.split(",");
      for (int i = 0; i < columnStringArr.length; i++){
        String[] familyAndColumn = columnStringArr[i].split(":");
        families.add(familyAndColumn[0]);
        columns.add(familyAndColumn[1]);
      }
    }
  
    public void map
    (
      ImmutableBytesWritable key, Result res,
      TableMapper<Text, IntWritable>.Context context
    ) throws java.io.IOException, InterruptedException
    {
      NavigableMap<byte[], NavigableMap<byte[], NavigableMap<Long, byte[]>>> map = null;
      map = res.getMap();
      ArrayList<String> resultValue = new ArrayList<String>();

    
    for (int i = 0; i < columns.size(); i++) {
      byte[] value = res.getValue(Bytes.toBytes(families.get(i)), Bytes.toBytes(columns.get(i)));
      if ( i <= 1  )
        resultValue.add("0");
      else resultValue.add(new String (value));
    }
     if (resultValue.get(0).length() == 0) return;
    context.write(
            new Text (String.join(",",resultValue.toArray(new String[]{}))),
            new IntWritable(1)
          );
    }
  }
  public static class ExtractForRandomForestReducer
    extends Reducer<Text,IntWritable,Text,IntWritable>{
    public void reduce
    (
      Text key,
      Iterable<IntWritable> putIterable,
      Reducer<Text, IntWritable, Text, IntWritable>.Context context
    ) throws java.io.IOException, InterruptedException {
      int count = 0;
      for (IntWritable t : putIterable) {
        context.write(key, t);
      }
    }
  }
}
