package correlation;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.hbase.mapreduce.TableOutputFormat;
import org.apache.hadoop.hbase.mapreduce.TableInputFormat;

import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.client.Scan;

public class ProjTool extends org.apache.hadoop.conf.Configured implements org.apache.hadoop.util.Tool {
  private int subTask;
  private FileSystem fs;
  private String tableName;

  public ProjTool(int subTaskIn, String tableNameIn){
    super();
    subTask = subTaskIn;
    tableName = tableNameIn;
  }
  
  public int run ( String[] args ) throws Exception {
      Configuration config = HBaseConfiguration.create();
      String originalFileInputPath = "input/chicagoCrime2001ToNow.csv";
      String accusedComplaintsInputPath = "input/accusedComplaints_from_2000_to_2018.csv";
      String complaintsInputPath = "input/complaints.csv";
      String crimeEntryTableName = "crimeTable";
      String complaintsTable = "complaintsTable";
      /*String outputPathPrefix = "";
      String task12InputPathString = "nguyen_trong_nguytk01/hw4/input/Books-sample-250records.csv";
      String task12OutputPathString = outputPathTask12Prefix;
      
      String task1OutputPathString = outputPathPrefix + "1";
      String task2OutputPathString = outputPathPrefix + "2";
      String task3OutputPathString = outputPathPrefix + "3";
      String task5OutputPathString = outputPathPrefix + "5";*/

      fs = FileSystem.get(config);
      
      if (subTask == 0) {
        runTaskImportCrime( originalFileInputPath, crimeEntryTableName, config );
      }
      
      if (subTask == 1) {
        runTaskImportComplaints( complaintsInputPath, complaintsTable, config );
      }
      
      if (subTask == 2) {
        runTaskImportAccusation( accusedComplaintsInputPath, complaintsTable, config );
      }
      
      if (subTask == 3) {
        runTaskExtractPlaceTimeCrimeType(
          crimeEntryTableName,
          "bigdata-proj/correlation/CommWeekCrimeCount",
          "CrimeLocation:CommArea,Time:month,Time:year,Attr:Type",
          "week",
          2000,
          2018,
          config,
          ExtractPlaceTimeCrimeType.ExtractPlaceTimeCrimeTypeMapper.class,
          ExtractPlaceTimeCrimeType.ExtractPlaceTimeCrimeTypeReducer.class
          );
      }
      if (subTask == 4) {
        runTaskExtractPlaceTimeCrimeType(
          crimeEntryTableName,
          "bigdata-proj/RandomForest/input",
          "Attr:Arrest,Attr:Domestic,PoliceLocation:Beat,Attr:Type,Time:month,Time:year,CrimeLocation:Street",
          "week",
          2000,
          2018,
          config,
          ExtractForRandomForest.ExtractForRandomForestMapper.class,
          ExtractForRandomForest.ExtractForRandomForestReducer.class);
      }
      
      return 1;
  }
  public void printHdfsFileContent(String path) throws java.io.IOException {
    java.io.InputStream stream = fs.open( new Path(path));
        java.util.Scanner scan = new java.util.Scanner(stream);
        while (scan.hasNextLine()) {
          System.out.println(scan.nextLine());
        }
        scan.close();
        stream.close();
  }
  
  public int runTaskImportCrime(String originalFileInputPath, String tableName, Configuration config) throws ClassNotFoundException, InterruptedException, java.io.IOException {
    config.set(TableOutputFormat.OUTPUT_TABLE, tableName);
    Job job = Job.getInstance(config, "Crime import");
    job.setJarByClass(App.class);
    job.setMapOutputValueClass(Put.class);
    job.setInputFormatClass(TextInputFormat.class);
    job.setMapperClass(correlation.CrimeEntryInput.ImportCrimeMapper.class);
    TableMapReduceUtil.initTableReducerJob(
      tableName,
      correlation.CrimeEntryInput.ImportCrimeReducer.class,
      job);
    FileInputFormat.addInputPath(
      job,
      new Path(originalFileInputPath));
    job.waitForCompletion(true);
    return 0;
  }
  
  public int runTaskImportComplaints (String originalFileInputPath, String tableName, Configuration config) throws ClassNotFoundException, InterruptedException, java.io.IOException {
    config.set(TableOutputFormat.OUTPUT_TABLE, tableName);
    Job job = Job.getInstance(config, "Complaints import");
    job.setJarByClass(App.class);
    job.setMapOutputValueClass(Put.class);
    job.setInputFormatClass(TextInputFormat.class);
    job.setMapperClass(Complaints.ComplaintsMapper.class);
    TableMapReduceUtil.initTableReducerJob(
      tableName,
      Complaints.ComplaintsReducer.class,
      job);
    FileInputFormat.addInputPath(
      job,
      new Path(originalFileInputPath));
    job.waitForCompletion(true);
    return 0;
  }
  
  public int runTaskExtractPlaceTimeCrimeType(String inputTableName, String outputFilePath, String columnsToScan, String timeUnit, int minYear, int maxYear, Configuration config, Class mapper, Class reducer) throws ClassNotFoundException, InterruptedException, java.io.IOException {
    int numOfPeriodsPerYear = 0;
    switch (timeUnit) {
      case "week":
        numOfPeriodsPerYear = 52;
        break;
      case "month":
        numOfPeriodsPerYear = 12;
        break;
      case "quarter":
        numOfPeriodsPerYear = 4;
        break;
      case "day":
        numOfPeriodsPerYear = 366;
        break;
    }
    config.set("numberOfPeriodsPerYear", new Integer(numOfPeriodsPerYear).toString());
    config.set("minYear", new Integer(minYear).toString());
    config.set("maxYear", new Integer(maxYear).toString());
    config.set("columnOrder", columnsToScan);

    config.set(TableInputFormat.INPUT_TABLE, inputTableName);
    
    Job job = Job.getInstance(config, "Extract Place Time Crime Type Count");
    job.setJarByClass(App.class);
    Scan scan = getScanWithAddedColumns(new Scan(), columnsToScan);
    TableMapReduceUtil.initTableMapperJob(
      inputTableName,
      scan,
      mapper,
      Text.class,
      IntWritable.class,
      job
    );
    FileSystem fs = FileSystem.get(config);
    fs.delete(new Path(outputFilePath),true);
    job.setReducerClass(reducer);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);
    
    FileOutputFormat.setOutputPath(job,new Path(outputFilePath));
    job.waitForCompletion(true);
    return 0;
  }
  
  
  
  public int runTaskImportAccusation(String originalFileInputPath, String tableName, Configuration config) throws ClassNotFoundException, InterruptedException, java.io.IOException {
    config.set(TableOutputFormat.OUTPUT_TABLE, tableName);
    Job job = Job.getInstance(config, "Accusation import");
    job.setJarByClass(App.class);
    job.setMapOutputValueClass(Put.class);
    job.setInputFormatClass(TextInputFormat.class);
    job.setMapperClass(AccusedComplaints.AccusedComplaintsMapper.class);
    TableMapReduceUtil.initTableReducerJob(
      tableName,
      AccusedComplaints.AccusedComplaintsReducer.class,
      job);
    FileInputFormat.addInputPath(
      job,
      new Path(originalFileInputPath));
    job.waitForCompletion(true);
    return 0;
  }
  
  public static Scan getScanWithAddedColumns(Scan scan, String query) {
    String[] columns = query.split(",");
    for (int i = 0; i < columns.length; i++){
      String[] familyAndColumn = columns[i].split(":");
      scan.addColumn(Bytes.toBytes(familyAndColumn[0]), Bytes.toBytes(familyAndColumn[1]));
    }
    return scan;
  }
}