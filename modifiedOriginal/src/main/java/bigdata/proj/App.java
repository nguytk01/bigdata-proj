package bigdata.proj;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class App {
  public static String delim = "=====";

  public static class MyMapperPhase1 extends Mapper<Object, Text, Text, Text> {
    private static int ID = 0;
    private static int Case_Number = 1;
    private static int Date = 2;
    private static int Block = 3;
    private static int IUCR = 4;
    private static int Primary_Type = 5;
    private static int Description = 6;
    private static int Location_Description = 7;
    private static int Arrest = 8;
    private static int Domestic = 9;
    private static int Beat = 10;
    private static int District = 11;
    private static int Ward = 12;
    private static int Community_Area = 13;
    private static int FBI_Code = 14;
    private static int X_Coordinate = 15;
    private static int Y_Coordinate = 16;
    private static int Year = 17;
    private static int Updated_On = 18;
    private static int Latitude = 19;
    private static int Longitude = 20;
    static int columnSize = 0;
    private CSVRecord row;
    private CSVParser parser;
    private DateTimeFormatter dateTimeParser;
    private DateTimeFormatter dateFormatter;
    private DateTimeFormatter timeFormatter;

    @Override
    public void setup(Context context) throws java.io.IOException, InterruptedException {
      dateTimeParser = DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm:ss a");
      dateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
      timeFormatter = DateTimeFormatter.ofPattern("hh:mm:ss a");
    }

    public void map(Object key, Text value, Context context)
        throws IOException, InterruptedException {
      parser =
          new CSVParser(
              new java.io.StringReader(value.toString()),
              CSVFormat.RFC4180.withRecordSeparator("\n").withIgnoreEmptyLines(true));
      List<CSVRecord> csvRecordList = parser.getRecords();
      row = csvRecordList.get(0);
      System.out.println(csvRecordList.toString());
      // System.out.println(row.toString());

      // the entry we are parsing is the header
      if (row.get(Date).equals("Date")) return;

      TemporalAccessor dateTime = dateTimeParser.parse(row.get(Date));
      String[] strArr =
          new String[] {
            row.get(ID),
            dateFormatter.format(dateTime),
            timeFormatter.format(dateTime),
            row.get(Block),
            row.get(Location_Description),
            row.get(Primary_Type),
            row.get(Latitude),
            row.get(Longitude),
            row.get(Arrest),
            row.get(Community_Area)
          };
      if (row.get(Latitude).length() == 0
          || row.get(Longitude).length() == 0
          || row.get(Community_Area).length() == 0) {
        context.write(new Text("1" + row.get(Primary_Type)), new Text(String.join(delim, strArr)));
      } else {
        context.write(new Text("0" + row.get(Primary_Type)), new Text(String.join(delim, strArr)));
      }
    }
  }

  public static class MyReducerPhase1 extends Reducer<Text, Text, Text, Text> {
    private MultipleOutputs mos;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
      mos = new MultipleOutputs(context);
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {}

    public void reduce(Text key, Iterable<Text> values, Context context)
        throws IOException, InterruptedException {
      String keyStr = key.toString();
      String namedOutput = "";
      switch (keyStr.charAt(0)) {
        case '0':
          namedOutput = "withEverything";
          break;
        case '1':
          namedOutput = "withoutAnyField";
          break;
      }
      for (Text t : values) {
        mos.write(namedOutput, key, t);
      }
    }
  }

  public static void main(String[] args) throws Exception {
    System.out.println("job");
    Configuration conf = new Configuration();
    FileSystem fs = FileSystem.get(conf);
    fs.delete(new Path("bigdata-proj/"), true);
    // conf.set("Maxsize", args[3]);
    Job job = Job.getInstance(conf, "Preprocess");
    job.setJarByClass(App.class);
    job.setMapperClass(MyMapperPhase1.class);
    job.setReducerClass(MyReducerPhase1.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);

    MultipleOutputs.addNamedOutput(
        job, "withEverything", TextOutputFormat.class, Text.class, Text.class);
    MultipleOutputs.addNamedOutput(
        job, "withoutAnyField", TextOutputFormat.class, Text.class, Text.class);

    FileInputFormat.addInputPath(job, new Path("input/chicagoCrime2001ToNow.csv"));
    FileOutputFormat.setOutputPath(job, new Path("bigdata-proj/"));
    System.out.println("job success(0)?" + (job.waitForCompletion(true) ? 0 : 1));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
    // --------------------------------------------------------------------------------------------------------
  }
}
