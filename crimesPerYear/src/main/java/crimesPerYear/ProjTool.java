package crimesPerYear;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class ProjTool extends org.apache.hadoop.conf.Configured
    implements org.apache.hadoop.util.Tool {
  private int subTask;
  private FileSystem fs;

  public ProjTool(int subTaskIn) {
    super();
    subTask = subTaskIn;
  }

  public int run(String[] args) throws Exception {
    Configuration config = new Configuration();
    String outputPathPrefix = "bigdata-proj-stats/";
    String task1InputPathString = "input/chicagoCrime2001ToNow.csv";
    String task1OutputPathString = outputPathPrefix + "1-byHour";

    String task2InputPathString = "bigdata-proj/withEverything-r-00000";
    String task2OutputPathString = outputPathPrefix + "2-byCommunityArea";

    String task3InputPathString = outputPathPrefix + "1/part-r-00000";
    String task3OutputPathString = outputPathPrefix + "3";

    String task5OutputPathString = outputPathPrefix + "5";
    if (subTask == 1) {
      runTask1(config, task1InputPathString, task1OutputPathString);
      printHdfsFileContent(task1OutputPathString + "/part-r-00000");
    }
    if (subTask == 2) {
      runTask2(config, task2InputPathString, task2OutputPathString);
      printHdfsFileContent(task2OutputPathString + "/part-r-00000");
    }
    if (subTask == 3) {
      runTask3(config, task3InputPathString, task3OutputPathString);
      printHdfsFileContent(task3OutputPathString + "/part-r-00000");
    }
    return 0;
  }

  public int runTask1(Configuration config, String inputFilePath, String outputPathString)
      throws Exception {
    FileSystem fs = FileSystem.get(config);
    fs.delete(new Path(outputPathString), true);
    // conf.set("Maxsize", args[3]);
    Job job = Job.getInstance(config, "Preprocess");
    job.setJarByClass(App.class);
    job.setMapperClass(crimesPerYear.CountByHour.MyMapperPhase1.class);
    job.setReducerClass(crimesPerYear.CountByHour.MyReducerPhase1.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(Text.class);
    FileInputFormat.addInputPath(job, new Path(inputFilePath));
    FileOutputFormat.setOutputPath(job, new Path(outputPathString));
    System.out.println("job success(0)?" + (job.waitForCompletion(true) ? 0 : 1));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
    return 0;
  }

  public int runTask2(Configuration config, String inputFilePath, String outputPathString)
      throws Exception {
    FileSystem fs = FileSystem.get(config);
    fs.delete(new Path(outputPathString), true);
    // conf.set("Maxsize", args[3]);
    Job job = Job.getInstance(config, "Count by Community");
    job.setJarByClass(App.class);
    job.setMapperClass(crimesPerYear.CountByCommunityArea.MyMapperPhase1.class);
    job.setReducerClass(crimesPerYear.CountByCommunityArea.MyReducerPhase1.class);
    job.setMapOutputKeyClass(IntWritable.class);
    job.setMapOutputValueClass(Text.class);
    job.setOutputKeyClass(IntWritable.class);
    job.setOutputValueClass(IntWritable.class);

    System.out.println(inputFilePath);
    System.out.println(outputPathString);

    FileInputFormat.addInputPath(job, new Path(inputFilePath));
    FileOutputFormat.setOutputPath(job, new Path(outputPathString));
    System.out.println("job success(0)?" + (job.waitForCompletion(true) ? 0 : 1));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
    return 0;
  }

  public int runTask3(Configuration config, String inputFilePath, String outputPathString)
      throws Exception {
    /*FileSystem fs = FileSystem.get(config);
    fs.delete(new Path(outputPathString), true);
    Job job = Job.getInstance(config, "Map Reduce Forecasting");
    job.setJarByClass(App.class);
    job.setMapperClass(HoltzWinters.MRF.MyMapperPhase1.class);
    job.setReducerClass(HoltzWinters.MRF.MyReducerPhase1.class);
    job.setMapOutputKeyClass(Text.class);
    job.setMapOutputValueClass(Text.class);
    job.setOutputKeyClass(IntWritable.class);
    job.setOutputValueClass(DoubleWritable.class);

    System.out.println(inputFilePath);
    System.out.println(outputPathString);

    FileInputFormat.addInputPath(job, new Path(inputFilePath));
    FileOutputFormat.setOutputPath(job, new Path(outputPathString));
    System.out.println("job success(0)?" + (job.waitForCompletion(true) ? 0 : 1));
    System.exit(job.waitForCompletion(true) ? 0 : 1);*/
    return 0;
  }

  public void printHdfsFileContent(String path) throws java.io.IOException {
    java.io.InputStream stream = fs.open(new Path(path));
    java.util.Scanner scan = new java.util.Scanner(stream);
    while (scan.hasNextLine()) {
      System.out.println(scan.nextLine());
    }
    scan.close();
    stream.close();
  }
}
