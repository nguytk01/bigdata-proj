package crimesPerYear;

public class Columns {
  private static int ID = 0;
  private static int Case_Number = 1;
  private static int Date = 2;
  private static int Block = 3;
  private static int IUCR = 4;
  private static int Primary_Type = 5;
  private static int Description = 6;
  private static int Location_Description = 7;
  private static int Arrest = 8;
  private static int Domestic = 9;
  private static int Beat = 10;
  private static int District = 11;
  private static int Ward = 12;
  private static int Community_Area = 13;
  private static int FBI_Code = 14;
  private static int X_Coordinate = 15;
  private static int Y_Coordinate = 16;
  private static int Year = 17;
  private static int Updated_On = 18;
  private static int Latitude = 19;
  private static int Longitude = 20;
}
