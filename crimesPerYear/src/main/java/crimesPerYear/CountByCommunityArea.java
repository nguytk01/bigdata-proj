package crimesPerYear;

import java.io.IOException;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

public class CountByCommunityArea {
  public static class MyMapperPhase1 extends Mapper<Object, Text, IntWritable, Text> {
    static int columnSize = 0;

    @Override
    public void setup(Context context) throws IOException, InterruptedException {}

    public void map(Object key, Text value, Context context)
        throws IOException, InterruptedException {
      // System.out.println(row.toString());
      String[] keyValue = value.toString().split("\\t");
      String[] cells = keyValue[1].toString().split("\\=\\=\\=\\=\\=");

      // the entry we are parsing is the header
      try {
        int communityArea = Integer.parseInt(cells[9]);
      } catch (Exception e) {
        throw new IOException(keyValue[1] + " " + cells[8]);
      }
      int communityArea = Integer.parseInt(cells[9]);
      context.write(new IntWritable(communityArea), new Text(""));
    }

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {}
  }

  public static class MyReducerPhase1 extends Reducer<IntWritable, Text, IntWritable, IntWritable> {
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {}

    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {}

    public void reduce(IntWritable key, Iterable<Text> values, Context context)
        throws IOException, InterruptedException {
      int count = 0;
      for (Text t : values) {
        count++;
      }
      context.write(key, new IntWritable(count));
    }
  }
}
