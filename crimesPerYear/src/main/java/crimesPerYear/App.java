package crimesPerYear;

public class App {

  public static void main(String[] args) throws Exception {
    int subTask = Integer.parseInt(args[1]);
    ProjTool projTool = new ProjTool(subTask);
    projTool.run(args);
  }
}
